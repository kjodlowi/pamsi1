#include <iostream>
#include <string>

#include "lib/data_storage.hh"

using namespace std;


int main(){

    list message;

    message.read("message.txt");

    cout << endl
         << "Projekt 1 - problem sortowania wiadomosci" << endl
         << "--------" << endl
         << "Wiadomosc przed uporzadkowaniem: " << endl
         << "--------" << endl;

    message.show_all();

    message.sort();

    cout << "--------" << endl
         << "Wiadomosc po uporzadkowaniu: " << endl
         << "--------" << endl;

    message.show_all();
    cout << endl;
    system("pause");
    return 0;
}