#include <iostream>
#include <string>
#include <fstream>

/**
 * * Pojedynczy element lity jednostronnej
 */
struct element
{
    int value; // w naszym przypadku służy do numeracji kolejnych części wiadomości
    std::string text; // treść wiadomości
    element* next; // wskaźnik na następny element
};

/**
 * * Lista jednokierunkowa
 */
class list
{  
private:
    element* head; //wskaźnik, który daje dostęp do listy
public:
    list(){head = NULL;};
    ~list(){ //destruktor, który usuwa wszystkie elementy listy
        while (head)
        {
            element* tmp = head;
            head = head->next;
            delete tmp;
        };   
    };

    bool isEmpty(){if (head == NULL) // metoda sprawdza czy lista jest pusta
    {
        return 1;
    }else return 0;
    };

    void add(element new_elem);
    void remove(void);
    void show_all(void);
    void sort(void);
    void read(std::string filename);
};

/**
 * * Funkcja dodawania elementów
 */
void list::add(element new_elem)
{
    element * elem = new element; //pomocniczy wskaźnik, którym tworzymy nowy element i wpisujemy do niego wartości
    elem->value = new_elem.value;
    elem->text = new_elem.text;
    elem->next = head;
    head = elem;
}

/**
 * * Funkcja usuwania elementów
 */
void list::remove(void)
{
    if(!isEmpty())
    {
        element *ptr; //pomocniczy wskaźnik, który służy do usuwania elementu
        ptr = head;
        head = head->next;
        delete ptr;
    }else{std::cout << "Lista jest pusta" << std::endl;}
}

/**
 * * Funkcja wyświetlania elementów
 * Złożoność obliczeniowa O(n)
 */
void list::show_all(void)
{
    if(!isEmpty())
    {
        element * n;
        n = head;
        while (n!=NULL){
        std::cout << n->text<< " ";
        n = (*n).next;
        }
        std::cout << std::endl;
    }

}

/**
 * * Funkcja sortowania elementów
 * zastosowano sortowanie babelkowe - zlozonosc obliczeniowa O(n^2)
 */
void list::sort(void)
{
    if (isEmpty()) exit(1); // sprawdzamy czy lista ma przynajmniej jeden element
    bool flag; // flaga, która informuje czy została wykonana jakaś zamiana, 
               //brak zamiany po ostatnim przejściu przez wszystkie elementy znaczy, że lista jest posortowana

    int tmp_val;  // pomocnicze zmienne do zamieniania miejscami numeru wiadomości i jej treści
    std::string tmp_str;

    element * n1; //dwa wskaźniki służące do skakania po elementach listy i porównywania ich
    element * n2;

    do
    {
        n1 = head; //elementy są ustawiane na początek listy
        n2 = head->next;
        flag = 0;
        
        while (n2 != NULL) //sprawdzamy czy n2 wskazuje na koniec listy
        {

            if (n1->value > n2->value) //porównywanie kolejności wiadomości
            {
                flag = 1; //zanotowanie operacji zamiany - trzeba przynajmniej jeszcze raz sprawdzić listę, by mieć pewność, że algorytm posortował

                tmp_val = n1->value; //zamiana wartości
                n1->value = n2->value;
                n2->value = tmp_val;

                tmp_str = n1->text;
                n1->text = n2->text;
                n2->text = tmp_str;
            }
            
            n1 = n2; //przeskoczenie na następne dwa elementy
            n2 = n2->next;
        }

    } while (flag > 0);
    
}


/**
 * * Funkcja dodawania elementow z pliku do listy
 * Złożoność obliczeniowa jest liniowa - O(n)
 */
void list::read(std::string filename)
{

    std::ifstream msg;
    msg.open(filename);

    if(!msg.is_open()) //sprawdzamy czy plik dobrze się otworzył
    {
        std::cout << "Blad przy otwieraniu pliku" << std::endl;
        exit(EXIT_FAILURE); 
    }

    element e;

    while (msg.good() && msg >> e.value >> e.text) //dodajemy elementy do listy tak długo jak w pliku znajdują się ponumerowane słowa
    {
        add(e);
    }
    
}